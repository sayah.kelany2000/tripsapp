import 'package:flutter/cupertino.dart';
import 'package:transportation_manager/modules/create_trips/presentation/controller/create_trips_cubit.dart';
import 'package:transportation_manager/modules/view_trips/presentation/controllers/home_cubit.dart';

abstract class ConstCubit{
  static CreateTripsCubit createTripsCubit(BuildContext context)=>CreateTripsCubit.get(context);
  static HomeCubit homeCubit(BuildContext context)=>HomeCubit.get(context);
}