import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/components/components.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/operations/operations.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/modules/view_trips/presentation/controllers/home_cubit.dart';

import 'string_const.dart';

class BottomNav extends StatelessWidget {
  const BottomNav({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        return Stack(
          alignment: Alignment.topRight,
          children: [
            BottomAppBar(
                height: 60.h,
                notchMargin: 8,
                shape:const AutomaticNotchedShape(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(1),
                    ),
                  ),
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                  ),
                ),
                clipBehavior: Clip.antiAlias,
                child: Padding(
                  padding: EdgeInsets.only(top: 8.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          ConstCubit.homeCubit(context).changeBottomNav(
                            index: 0,
                          );
                          ConstCubit.homeCubit(context)
                              .pageController
                              .animateToPage(0,
                                  duration: const Duration(milliseconds: 500),
                                  curve: Curves.easeInOut);
                        },
                        child: Components.customBottomItem(
                          icon: Icons.home_outlined,
                          text: home,
                          color: Operations.changeBottomColor(
                              context: context, index: 0),
                        ),
                      ),
                      SizedBox(
                        width: 40.w,
                      ),
                      GestureDetector(
                        onTap: () {
                          ConstCubit.homeCubit(context).changeBottomNav(
                            index: 2,
                          );
                          ConstCubit.homeCubit(context)
                              .pageController
                              .jumpToPage(2);
                        },
                        child: Components.customBottomItem(
                          icon: Icons.notifications_outlined,
                          text: notification,
                          color: Operations.changeBottomColor(
                              context: context, index: 2),
                        ),
                      ),
                      SizedBox(
                        width: 40.w,
                      ),
                      GestureDetector(
                        onTap: () {
                          ConstCubit.homeCubit(context).changeBottomNav(
                            index: 1,
                          );
                          // ConstCubit.homeCubit(context).pageController.jumpToPage(1);
                          ConstCubit.homeCubit(context)
                              .pageController
                              .animateToPage(
                                1,
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.easeInOut,
                              );
                        },
                        child: Components.customBottomItem(
                          icon: Icons.person_outline,
                          text: profile,
                          color: Operations.changeBottomColor(
                              context: context, index: 1),
                        ),
                      ),
                      SizedBox(
                        width: 40.w,
                      ),
                    ],
                  ),
                )),
            if (ConstCubit.homeCubit(context).index == 1 ||
                ConstCubit.homeCubit(context).index == 0)
              Positioned(
                right: ConstCubit.homeCubit(context).index == 1 ? 50.w : null,
                left: ConstCubit.homeCubit(context).index == 0 ? 115.5.w : null,
                bottom: 53.h,
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: const BoxDecoration(
                    color: AppColors.kButtonColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(100),
                      bottomRight: Radius.circular(100),
                    ),
                  ),
                ),
              )
          ],
        );
      },
    );
  }
}
