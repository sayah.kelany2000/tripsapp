
import 'package:flutter/material.dart';

abstract class AppColors{
 static const kPrimaryColor=Color(0xff2D2E74);
 static const kHeadColor=Color(0xff000D26);
 static const kFromToColor=Color(0xff000E27);
 static const kTextColor=Color(0xff6F77AB);
 static const kTimeAndDateColor=Color(0xffA2A8CC);
 static const kScaffoldBackGroundColor=Color(0xffF5F6F8);
 static const kBorderColor=Color(0xff707070);
 static const kBottomColor=Color(0xff727C8E);
 static const kButtonColor=Color(0xffEB8623);
 static const kDotesColor=Color(0xffCBCEE0);
 static const kDividerColor=Color(0xffE4E4E4);
 static const kEmailColor=Color(0xffA3A3B2);
 static const kCreatedColor=Color(0xff515C6F);
 static const kArrowColor=Color(0xffB7BBD3);

}