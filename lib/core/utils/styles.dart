import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/color.dart';

abstract class Styles
{
static TextStyle title18=TextStyle(
  fontSize: 18.sp,
  fontWeight: FontWeight.w600,
  color: Colors.white
);
static TextStyle title18BlueColor=TextStyle(
    fontSize: 18.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kHeadColor
);
static TextStyle nameSemiBold30=TextStyle(
    fontSize: 24.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kHeadColor
);
static TextStyle place17=TextStyle(
  fontSize: 17.sp,
  fontWeight: FontWeight.w700,
  color: AppColors.kPrimaryColor
);
static TextStyle semiBold17=TextStyle(
    fontSize: 17.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kPrimaryColor
);
static TextStyle date15=TextStyle(
    fontSize: 15.sp,
    fontWeight: FontWeight.w500,
    color: AppColors.kTextColor
);
static TextStyle date13SemiBold=TextStyle(
    fontSize: 13.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kTimeAndDateColor
);
static TextStyle font12SemiBold=TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kTimeAndDateColor
);
static TextStyle regular17SegoeUI=TextStyle(
    fontSize: 17.sp,
    fontFamily: 'Segoe UI',
    fontWeight: FontWeight.w400,
    color: AppColors.kFromToColor
);
static TextStyle view12SemiBold=TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kPrimaryColor
);
static TextStyle bottomBold12({required Color color})=>TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
    color: color,
);
static TextStyle backAndResetSemi20=TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kButtonColor
);
static TextStyle medium15=TextStyle(
    fontSize: 15.sp,
    fontWeight: FontWeight.w500,
    color: AppColors.kPrimaryColor
);static TextStyle medium15WhiteColor=TextStyle(
    fontSize: 15.sp,
    fontWeight: FontWeight.w500,
    color: Colors.white
);
static TextStyle semiBold30=TextStyle(
    fontSize: 20.sp,
    fontWeight: FontWeight.w600,
    color: Colors.white
);
static TextStyle semiBold16=TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kEmailColor,
);
static TextStyle semiBold33=TextStyle(
    fontSize: 33.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kCreatedColor,
);
static TextStyle regular15=TextStyle(
    fontSize: 15.sp,
    fontWeight: FontWeight.w400,
    color: AppColors.kCreatedColor,
);
static TextStyle semiBold20=TextStyle(
    fontSize: 20.sp,
    fontWeight: FontWeight.w600,
    color: Colors.white,
);
static TextStyle semiBold15=TextStyle(
    fontSize: 15.sp,
    fontWeight: FontWeight.w600,
    color: AppColors.kPrimaryColor,
);
}