import 'package:get/get.dart';
import 'package:transportation_manager/modules/create_trips/presentation/page/created_successfully_page.dart';
import 'package:transportation_manager/modules/profile/presentation/page/profile_page.dart';
import 'package:transportation_manager/modules/view_trips/presentation/page/home_page.dart';
import 'package:transportation_manager/modules/create_trips/presentation/page/create_trips_page.dart';
import 'package:transportation_manager/modules/view_trips/presentation/page/main_page.dart';
import 'package:transportation_manager/modules/view_trips/presentation/page/trip_details_page.dart';

List<GetPage<dynamic>>? get getPages =>
    [
      MainPage.page,
      CreatedSuccessfullyPage.page,
      HomePage.page,
      CreateTripsPage.page,
      ProfilePage.page,
      TripDetailsPage.page,
    ];
