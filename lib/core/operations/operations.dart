import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/utils/color.dart';

import '../../modules/create_trips/presentation/controller/create_trips_cubit.dart';

abstract class Operations
{
  static AlignmentDirectional compareToMove({required BuildContext context,required double y})=>CreateTripsCubit.get(context).isFrom
      ? AlignmentDirectional(-1, y.h)
      :const AlignmentDirectional(-1, -1);
  static Color changeColorSwitchButton(BuildContext context)=>ConstCubit.createTripsCubit(context).isFrom?Colors.grey:AppColors.kButtonColor;

  static void selectTimeOrDate({required bool? isDate,required bool? isStart,required BuildContext context}){
    if(isDate!=null) {
      if (isDate) {
        ConstCubit.createTripsCubit(context).showDate(context);
      }
    }
    if(isStart!=null) {
      if (isStart) {
        ConstCubit.createTripsCubit(context).showTime(context);
      } else {
        ConstCubit.createTripsCubit(context).showTimeEnd(context);
      }
    }
  }
  static Color changeBottomColor({
   required BuildContext context,
    required int index
  })=>ConstCubit.homeCubit(context).index == index
      ? AppColors.kButtonColor
      : AppColors.kBottomColor;
}