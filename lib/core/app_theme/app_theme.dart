import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:transportation_manager/core/utils/color.dart';

abstract class AppTheme
{
  static ThemeData lightMode=ThemeData.light().copyWith(
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: AppColors.kButtonColor,
    ),
    scaffoldBackgroundColor: AppColors.kScaffoldBackGroundColor,
    textTheme: GoogleFonts.quicksandTextTheme(
      ThemeData.light().textTheme,
    ),
    colorScheme: ColorScheme.fromSeed(
      seedColor: AppColors.kHeadColor,
    ),
  );
}