import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';

abstract class Components {
  static Widget customDivider({required double endIndent}) => Divider(
        endIndent: endIndent,
        color: Colors.grey,
        height: 5,
      );

  static Widget customCircleOutLined({required Color color}) => Container(
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
              width: 4,
              color: color,
            )),
        child: Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                width: 4,
                color: Colors.white,
              )),
        ),
      );

  static Widget customButton() => Padding(
        padding: EdgeInsets.only(bottom: 60.h),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: AppColors.kButtonColor,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 55),
              child: Text(
                create,
                style: Styles.semiBold30,
              ),
            ),
          ),
        ),
      );

  static Widget customBottomItem(
          {required IconData icon, required String text,required Color color}) =>
      Column(
        children: [
          Icon(
            icon,
            color: color,
            size: 26,
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            text,
            style: Styles.bottomBold12(color: color),
          )
        ],
      );
}
