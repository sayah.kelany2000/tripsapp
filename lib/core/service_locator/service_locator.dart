import 'package:get_it/get_it.dart';
import 'package:transportation_manager/modules/view_trips/data/data_source/view_trips_data_source.dart';
import 'package:transportation_manager/modules/view_trips/data/repo/view_trps_repo.dart';
import 'package:transportation_manager/modules/view_trips/domain/usecases/view_trips_usecase.dart';

final getIt=GetIt.instance;
abstract class ServiceLocator
{
 static void initService()
  {
    ///FoR UseCase</>
    getIt.registerLazySingleton<ViewTripsUseCase>(() => ViewTripsUseCase(baseViewTripsRepo: getIt()));

    ///FoR Repository<..>
    getIt.registerLazySingleton<ViewTripsRepo>(() => ViewTripsRepo(baseViewTripsDataSource: getIt()));
    
    /// FoR DataSource<..>
    getIt.registerLazySingleton<ViewTripsDataSource>(() => ViewTripsDataSource());
  }
}