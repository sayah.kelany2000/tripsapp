import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:transportation_manager/core/app_theme/app_theme.dart';
import 'package:transportation_manager/core/pages/pages.dart';
import 'package:transportation_manager/modules/create_trips/presentation/controller/create_trips_cubit.dart';
import 'package:transportation_manager/modules/view_trips/presentation/controllers/home_cubit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CreateTripsCubit(),
        ),
        BlocProvider(
          create: (context) => HomeCubit(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (_, child) {
          return GetMaterialApp(
            getPages: getPages,
            debugShowCheckedModeBanner: false,
            theme: AppTheme.lightMode,

          );
        },
      ),
    );
  }
}
