import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/operations/operations.dart';
import 'package:transportation_manager/core/utils/const_svg.dart';
import 'package:transportation_manager/modules/create_trips/presentation/controller/create_trips_cubit.dart';

class BuildSwitchButton extends StatelessWidget {
  const BuildSwitchButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 35, top: 8, bottom: 8),
      child: InkWell(
        onTap: () {
          ConstCubit.createTripsCubit(context).changeFromTo();
        },
        child: BlocBuilder<CreateTripsCubit, CreateTripsState>(
          builder: (context, state) {
            return Container(
              decoration: BoxDecoration(
                  color: Operations.changeColorSwitchButton(context),
                  borderRadius: BorderRadius.circular(15)),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: SvgPicture.asset(
                  arrowCompare,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
