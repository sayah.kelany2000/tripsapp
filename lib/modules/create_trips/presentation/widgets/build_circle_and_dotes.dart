import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/components/components.dart';
import 'package:transportation_manager/core/utils/color.dart';

class BuildCircleAndDotes extends StatelessWidget {
  const BuildCircleAndDotes({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Components.customCircleOutLined(
          color: AppColors.kButtonColor,
        ),
        SizedBox(
          height: 3.h,
        ),
        DottedLine(
          dashGapLength: 5.h,
          lineLength: 44.h,
          direction: Axis.vertical,
          dashColor: AppColors.kDotesColor,
          dashRadius: 5,
          dashLength: 5,
          lineThickness: 5,
        ),
        SizedBox(
          height: 1.h,
        ),
        Components.customCircleOutLined(
          color: AppColors.kHeadColor,
        ),
      ],
    );
  }
}
