import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:transportation_manager/core/components/components.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/operations/operations.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';
import 'package:transportation_manager/modules/create_trips/presentation/controller/create_trips_cubit.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/build_choose_time.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/build_row_choose_time.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/build_switch_button.dart';

class BuildTextField extends StatelessWidget {
  const BuildTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            from.toUpperCase(),
            style: Styles.font12SemiBold,
          ),
          SizedBox(
            height: 3.h,
          ),
          BlocBuilder<CreateTripsCubit, CreateTripsState>(
            builder: (context, state) {
              return AnimatedAlign(
                heightFactor: 1.1,
                widthFactor: 1,
                duration: const Duration(milliseconds: 500),
                alignment: Operations.compareToMove(context: context, y: 65),
                child: Text(
                  'ipu'.toUpperCase(),
                  style: Styles.regular17SegoeUI,
                ),
              );
            },
          ),
          Components.customDivider(endIndent: 40),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      to.toUpperCase(),
                      style: Styles.font12SemiBold,
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    BlocBuilder<CreateTripsCubit, CreateTripsState>(
                      builder: (context, state) {
                        return AnimatedAlign(
                          widthFactor: 1,
                          heightFactor: 1.1,
                          duration: const Duration(milliseconds: 500),
                          alignment: Operations.compareToMove(
                              context: context, y: -65),
                          child: Text(
                            'RUKN ALDEN'.toUpperCase(),
                            style: Styles.regular17SegoeUI,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              const Spacer(),
              const BuildSwitchButton(),
            ],
          ),
          Components.customDivider(endIndent: 40),
          SizedBox(
            height: 20.h,
          ),
          const BuildRowChooseTime(),
          SizedBox(height: 10.h,),
          BlocBuilder<CreateTripsCubit, CreateTripsState>(
            builder: (context, state) {
              return BuildChooseTime(
                isDate: true,
                text: date,
                timeOrDate:
                DateFormat('yyyy-MM-dd').format(ConstCubit
                    .createTripsCubit(context)
                    .dateTime),
              );
            },
          ),
        ],
      ),
    );
  }
}
