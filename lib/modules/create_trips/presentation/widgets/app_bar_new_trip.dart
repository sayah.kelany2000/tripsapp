import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';

class AppBarNewTrip extends StatelessWidget {
  const AppBarNewTrip({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only(bottom: 10,top: 115.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            back,
            style: Styles.backAndResetSemi20,
          ),
          Text(
            newTrip.toUpperCase(),
            style: Styles.title18,
          ),
          Text(
            reset,
            style: Styles.backAndResetSemi20,
          ),
        ],
      ),
    );
  }
}
