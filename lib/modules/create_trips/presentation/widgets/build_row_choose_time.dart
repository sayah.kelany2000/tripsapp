import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/modules/create_trips/presentation/controller/create_trips_cubit.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/build_choose_time.dart';

class BuildRowChooseTime extends StatelessWidget {
  const BuildRowChooseTime({super.key});

  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        Expanded(
          child: BlocBuilder<CreateTripsCubit, CreateTripsState>(
            builder: (context, state) {
              return BuildChooseTime(
                isStart: true,
                text: starTime,
                timeOrDate: ConstCubit
                    .createTripsCubit(context)
                    .timeOfDayStart
                    .format(context)
                    .toString(),
              );
            },
          ),
        ),
        Expanded(
          child: BlocBuilder<CreateTripsCubit, CreateTripsState>(
            builder: (context, state) {
              return BuildChooseTime(
                isStart: false,
                text: endTime,
                timeOrDate: ConstCubit
                    .createTripsCubit(context)
                    .timeOfDayEnd
                    .format(context)
                    .toString(),
              );
            },
          ),
        ),
      ],
    );
  }
}
