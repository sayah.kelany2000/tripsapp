import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/build_circle_and_dotes.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/build_text_field.dart';

class CardNewTrip extends StatelessWidget {
  const CardNewTrip({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(

      top: 170.h,
      width: 361.w,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 25.w),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              35,
            ),
          ),
          child: Padding(
            padding:  EdgeInsets.only(left: 40.w,top: 50.h,bottom: 55.h),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 8, top: 22.h),
                  child:const BuildCircleAndDotes(),
                ),
               const BuildTextField(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
