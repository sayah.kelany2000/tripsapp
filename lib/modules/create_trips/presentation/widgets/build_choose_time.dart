import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/components/components.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/operations/operations.dart';
import 'package:transportation_manager/core/utils/styles.dart';

class BuildChooseTime extends StatelessWidget {
  const BuildChooseTime({
    super.key,
    required this.text,
    required this.timeOrDate,
    this.isStart,
    this.isDate=false,
  });

  final String text;
  final String timeOrDate;
  final bool? isStart;
  final bool ?isDate ;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          text,
          style: Styles.font12SemiBold,
        ),
        SizedBox(
          height: 5.h,
        ),
        GestureDetector(
          onTap: () {
            Operations.selectTimeOrDate(isDate: isDate, isStart: isStart, context: context);
          },
          child: Text(
            timeOrDate,
            style: Styles.medium15,
          ),
        ),
        Components.customDivider(endIndent: isDate!?159.5.w:40.w)
      ],
    );
  }
}
