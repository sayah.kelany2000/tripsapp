import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'create_trips_state.dart';

class CreateTripsCubit extends Cubit<CreateTripsState> {
  CreateTripsCubit() : super(CreateTripsInitial());

  static CreateTripsCubit get(BuildContext context) => BlocProvider.of(context);

  bool isFrom = false;

  void changeFromTo() {
    isFrom = !isFrom;
    emit(CreateTripsChangeFromToState());
  }

  DateTime dateTime = DateTime.now();

  void showDate(BuildContext context) {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(DateTime.now().year + 1),
    ).then((value) {
      if(value!=null) {
        dateTime = value;
        emit(CreateTripsChangeDateState());
      }
    });
  }
  TimeOfDay timeOfDayStart=TimeOfDay(hour: TimeOfDay.now().hour, minute: TimeOfDay.now().minute);
  TimeOfDay timeOfDayEnd=TimeOfDay(hour: TimeOfDay.now().hour, minute: TimeOfDay.now().minute);
  void showTime(BuildContext context) {
    showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    ).then((value) {
      if(value!=null) {
        timeOfDayStart = value;
        emit(CreateTripsChangeTimeState());
      }
    });
  }
  void showTimeEnd(BuildContext context) {
    showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    ).then((value) {
      if(value!=null) {
        timeOfDayEnd = value;
        emit(CreateTripsChangeEndTimeState());
      }
    });
  }
}
