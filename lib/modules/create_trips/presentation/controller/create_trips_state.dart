part of 'create_trips_cubit.dart';

@immutable
abstract class CreateTripsState {}

class CreateTripsInitial extends CreateTripsState {}
class CreateTripsChangeFromToState extends CreateTripsState {}
class CreateTripsChangeDateState extends CreateTripsState {}
class CreateTripsChangeTimeState extends CreateTripsState {}
class CreateTripsChangeEndTimeState extends CreateTripsState {}
