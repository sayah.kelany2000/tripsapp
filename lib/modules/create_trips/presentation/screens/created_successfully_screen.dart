import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/core/utils/const_svg.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';
import 'package:transportation_manager/modules/create_trips/presentation/page/create_trips_page.dart';

class CreatedSuccessfullyScreen extends StatelessWidget {
  const CreatedSuccessfullyScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(left: 45.w, right: 45.w, top: 125.h),
          child: ListView(
            physics: const ClampingScrollPhysics(),
            children: [
              Lottie.asset(
                successLottie(
                  'success.json',
                ),
                width: 150.w,
                height: 150.h,
                repeat: false,
              ),
              SizedBox(
                height: 31.h,
              ),
              Text(
                tripCreated,
                style: Styles.semiBold33,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 16.h,
              ),
              Text(
                longText,
                style: Styles.regular15,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 53.h,
              ),
              GestureDetector(
                onTap: (){
                  Get.offAndToNamed(CreateTripsPage.name);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: AppColors.kButtonColor,
                  ),
                  child: Padding(
                    padding:  EdgeInsets.only(left: 25.w,top: 15.h,bottom: 15.h),
                    child: Row(
                      children: [
                        Text(
                          createAnotherTrip,
                          style: Styles.semiBold20,
                        ),
                        SizedBox(
                          width: 19.w,
                        ),
                        SvgPicture.asset(arrowRightFill,)
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
