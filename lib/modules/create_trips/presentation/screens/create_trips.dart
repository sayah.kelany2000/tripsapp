import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transportation_manager/core/components/components.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/modules/create_trips/presentation/page/created_successfully_page.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/app_bar_new_trip.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/card_new_trip.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/head_new_trip.dart';

class CreateTrip extends StatelessWidget {
  const CreateTrip({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            color: AppColors.kScaffoldBackGroundColor,
          ),
          const HeadNewTrip(),
          const AppBarNewTrip(),
          const CardNewTrip(),
          GestureDetector(
            onTap: () {
              Get.offAndToNamed(CreatedSuccessfullyPage.name);
            },
            child: Components.customButton(),
          ),
        ],
      ),
    );
  }
}
