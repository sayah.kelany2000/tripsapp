import 'package:get/get.dart';
import 'package:transportation_manager/modules/create_trips/presentation/screens/create_trips.dart';

class CreateTripsPage {
  static const String name = '/create_trips';
  static GetPage page = GetPage(
    name: name,
      transition: Transition.rightToLeft,
      // curve: Curves.easeOutBack,

      transitionDuration: const Duration(milliseconds: 300),
    page: () => const CreateTrip(),
  );
}
