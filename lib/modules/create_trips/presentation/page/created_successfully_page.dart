import 'package:get/get.dart';
import 'package:transportation_manager/modules/create_trips/presentation/screens/created_successfully_screen.dart';

class CreatedSuccessfullyPage {
  static const String name = '/created_successfully';
  static GetPage page = GetPage(
    name: name,
    transition: Transition.fadeIn,
    // curve: Curves.easeOutBack,

    transitionDuration: const Duration(milliseconds: 300),
    page: () => const CreatedSuccessfullyScreen(),
  );
}
