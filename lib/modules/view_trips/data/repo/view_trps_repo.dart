import 'package:dartz/dartz.dart';
import 'package:transportation_manager/core/error/exception.dart';
import 'package:transportation_manager/core/error/failure.dart';
import 'package:transportation_manager/modules/view_trips/data/data_source/view_trips_data_source.dart';
import 'package:transportation_manager/modules/view_trips/domain/entity/view_trips_entity.dart';
import 'package:transportation_manager/modules/view_trips/domain/repo/view_trips_repo.dart';

class GetDataRepo extends BaseGetDataRepo {
  final BaseGetDataDataSource baseGetDataDataSource;

  GetDataRepo(this.baseGetDataDataSource);

  @override
  Future<Either<Failure, List<ViewTripsEntity>>> getData() async {
    try {
      return Right(await baseGetDataDataSource.getData());
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorModel.message));
    }
  }
}

class ViewTripsRepo extends BaseViewTripsRepo {
  final BaseViewTripsDataSource baseViewTripsDataSource;

  ViewTripsRepo({required this.baseViewTripsDataSource});

  @override
  Future<Either<Failure, List<ViewTripsEntity>>> getData() async {
    try {
      return Right(await baseViewTripsDataSource.getData());
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorModel.message));
    }
  }

  @override
  Future<Either<Failure, List<ViewTripsEntity>>> getTrips() async {
    try {
      return Right(await baseViewTripsDataSource.getViewTrips());
    } on ServerException catch (failure) {
      return Left(ServerFailure(failure.errorModel.message));
    }
  }
}
