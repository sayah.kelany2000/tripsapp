import 'package:transportation_manager/modules/view_trips/domain/entity/view_trips_entity.dart';

class ViewTripsModel extends ViewTripsEntity {
  const ViewTripsModel({
    required super.beginPlace,
    required super.endPlace,
    required super.tripDate,
    required super.tripTime,
    required super.availableTrip,
  });

  factory ViewTripsModel.fromJson(Map<String, dynamic> json) {
    return ViewTripsModel(
      beginPlace: json['begin_place'],
      endPlace: json['end_place'],
      tripDate: json['trip_date'],
      tripTime: json['trip_time'],
      availableTrip: json['available_trip'],
    );
  }
}
