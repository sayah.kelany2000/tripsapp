import 'package:dio/dio.dart';
import 'package:transportation_manager/core/error/exception.dart';
import 'package:transportation_manager/core/network/error_model.dart';
import 'package:transportation_manager/modules/view_trips/data/model/view_trips_model.dart';
abstract class BaseGetDataDataSource
{
  Future<List<ViewTripsModel>> getData();
}
abstract class BaseViewTripsDataSource
{
  Future<List<ViewTripsModel>> getViewTrips();
  Future<List<ViewTripsModel>> getData();
}
class ViewTripsDataSource extends BaseViewTripsDataSource
{
  @override
  Future< List<ViewTripsModel>> getViewTrips()async {
    final response=await Dio().get('path');
    if(response.statusCode==200)
      {
        return List<ViewTripsModel>.from((response.data[''] as List).map((e) => ViewTripsModel.fromJson(e)));
      }
    else{
      throw ServerException(ErrorModel.fromJson(response.data));
    }
  }

  @override
  Future<List<ViewTripsModel>> getData() {
    // TODO: implement getData
    throw UnimplementedError();
  }

}