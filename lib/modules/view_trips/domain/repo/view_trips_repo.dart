import 'package:dartz/dartz.dart';
import 'package:transportation_manager/core/error/failure.dart';
import 'package:transportation_manager/modules/view_trips/domain/entity/view_trips_entity.dart';

abstract class BaseViewTripsRepo
{
  Future<Either<Failure,List<ViewTripsEntity>>> getTrips();
  Future<Either<Failure,List<ViewTripsEntity>>> getData();
}
abstract class BaseGetDataRepo
{
  Future<Either<Failure,List<ViewTripsEntity>>> getData();
}