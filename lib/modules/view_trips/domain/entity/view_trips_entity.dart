import 'package:equatable/equatable.dart';

class ViewTripsEntity extends Equatable {
  final String beginPlace;
  final String endPlace;
  final String tripDate;
  final String tripTime;
  final String availableTrip;

  const ViewTripsEntity({
    required this.beginPlace,
    required this.endPlace,
    required this.tripDate,
    required this.tripTime,
    required this.availableTrip,
  });

  @override
  List<Object> get props => [
        beginPlace,
        endPlace,
        tripDate,
        tripTime,
        availableTrip,
      ];
}
