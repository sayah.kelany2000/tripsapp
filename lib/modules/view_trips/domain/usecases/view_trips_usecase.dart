import 'package:dartz/dartz.dart';
import 'package:transportation_manager/core/base_use_case/base_use_case.dart';
import 'package:transportation_manager/core/error/failure.dart';
import 'package:transportation_manager/modules/view_trips/domain/entity/view_trips_entity.dart';
import 'package:transportation_manager/modules/view_trips/domain/repo/view_trips_repo.dart';

 class ViewTripsUseCase extends BaseUseCase<List<ViewTripsEntity>,NoParameters>
{
  final BaseViewTripsRepo baseViewTripsRepo;

  ViewTripsUseCase({required this.baseViewTripsRepo});
  @override
  Future<Either<Failure, List<ViewTripsEntity>>> call(NoParameters parameters) {
    return baseViewTripsRepo.getTrips();
  }

}