import 'package:get/get.dart';
import 'package:transportation_manager/modules/view_trips/presentation/screens/main_screen.dart';

class MainPage {
  static const String name = '/';
  static GetPage page = GetPage(
    name: name,

    page: () => const MainScreen(),
  );
}
