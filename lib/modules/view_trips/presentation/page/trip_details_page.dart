import 'package:flutter/animation.dart';
import 'package:get/get.dart';
import 'package:transportation_manager/modules/view_trips/presentation/screens/trip_details.dart';

class TripDetailsPage {
  static const String name = '/trip_details';
  static GetPage page = GetPage(
    name: name,
    transitionDuration:const Duration(
      milliseconds: 500,
    ),
    curve: Curves.easeInOut,
    page: () => const TripDetailsScreen(),
  );
}
