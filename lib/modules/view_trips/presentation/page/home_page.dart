import 'package:get/get.dart';
import 'package:transportation_manager/modules/view_trips/presentation/screens/home.dart';

class HomePage {
  static const String name = '/home';
  static GetPage page = GetPage(
    name: name,

    page: () => const HomeScreen(),
  );
}
