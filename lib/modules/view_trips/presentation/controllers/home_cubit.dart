import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transportation_manager/modules/profile/presentation/screens/profile_screen.dart';
import 'package:transportation_manager/modules/view_trips/presentation/screens/home.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  static HomeCubit get(BuildContext context) => BlocProvider.of(context);
  int index = 0;

  void changeBottomNav({required int index}) {
    this.index = index;
    emit(HomeChangeBottomNavState());
  }

  List<Widget> pages = [
    const HomeScreen(),
    const ProfileScreen(),
  ];
 late PageController pageController=PageController(
    initialPage: index,
  );
}
