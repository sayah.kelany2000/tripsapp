import 'package:flutter/material.dart';
import 'package:transportation_manager/modules/view_trips/presentation/widgets/trip_details_widgets/seats_row.dart';

class SeatsItem extends StatelessWidget {
  const SeatsItem({super.key});

  @override
  Widget build(BuildContext context) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        SeatsRow(),
        SeatsRow(),
      ],
    );
  }
}
