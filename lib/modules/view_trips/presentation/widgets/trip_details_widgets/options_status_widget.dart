import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';

class OptionsStatusWidget extends StatelessWidget {
  const OptionsStatusWidget({super.key,required this.color,required this.text});
  final Color color;
  final String text;
  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        CircleAvatar(
          radius: 10,
          backgroundColor: color,
        ),
        SizedBox(
          width: 10.w,
        ),
        Text(
          text,
          style: Styles.medium15WhiteColor,
        ),
      ],
    );
  }
}
