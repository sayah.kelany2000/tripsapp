import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/core/utils/styles.dart';

class SeatsRow extends StatelessWidget {
  const SeatsRow({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              15,
            ),
            color: AppColors.kDotesColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'A1',
              style: Styles.semiBold17,
            ),
          ),
        ),
        SizedBox(
            width:26.w
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              15,
            ),
            color: AppColors.kDotesColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'A1',
              style: Styles.semiBold17,
            ),
          ),
        ),
      ],
    );
  }
}
