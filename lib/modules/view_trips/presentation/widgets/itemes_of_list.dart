import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:transportation_manager/core/utils/const_svg.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';
import 'package:transportation_manager/modules/view_trips/presentation/page/trip_details_page.dart';

class BuildItemsOfList extends StatelessWidget {
  const BuildItemsOfList({super.key});

  @override
  Widget build(BuildContext context) {
    return  Card(
      shape:
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.only(left: 32,top: 15),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      place.toUpperCase(),
                      style: Styles.place17,
                    ),
                    SizedBox(
                      width: 52.w,
                    ),
                    Text(
                      ipu.toUpperCase(),
                      style: Styles.place17,
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(ellipse),
                    SizedBox(
                      width: 6.w,
                    ),
                    SvgPicture.asset(divider),
                    SvgPicture.asset(
                      location,
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      date,
                      style: Styles.date15,
                    ),
                    SizedBox(
                      width: 124.w,
                    ),
                    Text(
                      time,
                      style: Styles.date15,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  children: [
                    Text(
                      fakeDate,
                      style: Styles.date13SemiBold,
                    ),
                    SizedBox(
                      width: 98.w,
                    ),
                    Text(
                      fakeTime,
                      style: Styles.date13SemiBold,
                    ),
                  ],
                ),
                SizedBox(
                  height: 12.h,
                ),
                Text(
                  '$availableSeats 14',
                  style: Styles.date15,
                )
              ],
            ),
            SizedBox(
              width: 20.w,
            ),
            SvgPicture.asset(dividerVertical),

            SizedBox(
              width: 25.w,
            ),
            Container(

              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey.withOpacity(0.6)),
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Padding(
                padding: const EdgeInsets.only(left: 12,right: 14,bottom: 12,top: 14.5),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: (){
                        Get.toNamed(TripDetailsPage.name);
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 12,
                        child: SvgPicture.asset(arrowRight),
                      ),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),

                    Text(
                      view.toUpperCase(),
                      style: Styles.view12SemiBold,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
