import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:transportation_manager/core/utils/const_svg.dart';
import 'package:transportation_manager/modules/view_trips/presentation/widgets/appbar_widget.dart';

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: 160.h,
          child: SvgPicture.asset(
            headViewTrips,
            width: double.infinity,
            fit: BoxFit.fill,
          ),
        ),
       const AppBarWidget(),
      ],
    );
  }
}
