import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/core/utils/const_svg.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';
import 'package:transportation_manager/modules/view_trips/presentation/widgets/trip_details_widgets/options_status_widget.dart';
import 'package:transportation_manager/modules/view_trips/presentation/widgets/trip_details_widgets/seats_item.dart';

class TripDetailsScreen extends StatelessWidget {
  const TripDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: 60.h,
              right: 35.w,
              bottom: 19.h,
              left: 35.w,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SvgPicture.asset(
                  arrowBackOutLined,
                ),
                Text(
                  tripDetails.toUpperCase(),
                  style: Styles.title18BlueColor,
                ),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    SvgPicture.asset(
                      circle,
                    ),
                    SvgPicture.asset(
                      plus,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                  color: AppColors.kHeadColor,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(40),
                    topLeft: Radius.circular(40),
                  )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      left: 30.w,
                      right: 30.w,
                      top: 30.h,
                      bottom: 15.h,
                    ),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        OptionsStatusWidget(
                          color: AppColors.kButtonColor,
                          text: selected,
                        ),
                        OptionsStatusWidget(
                          color: AppColors.kPrimaryColor,
                          text: booked,
                        ),
                        OptionsStatusWidget(
                          color: AppColors.kDotesColor,
                          text: available,
                        ),
                      ],
                    ),
                  ),
                  const Divider(
                    color: AppColors.kDividerColor,
                  ),
                  Expanded(
                    child: ListView.separated(
                      itemBuilder: (context, index) => const SeatsItem(),
                      separatorBuilder: (context, index) => SizedBox(
                        height: 25.h,
                      ),
                      itemCount: 10,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
