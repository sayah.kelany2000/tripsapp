import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:transportation_manager/core/const_cubit/const_cubit.dart';
import 'package:transportation_manager/core/utils/bottom_nav.dart';
import 'package:transportation_manager/modules/create_trips/presentation/page/create_trips_page.dart';
import 'package:transportation_manager/modules/view_trips/presentation/controllers/home_cubit.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
  builder: (context, state) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed(
            CreateTripsPage.name,
          );
        },
        child: const Icon(
          Icons.add,
          size: 40,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      bottomNavigationBar: const BottomNav(),
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        controller: ConstCubit.homeCubit(context).pageController,
        children: ConstCubit.homeCubit(context).pages,
      ),
    );
  },
);
  }
}
