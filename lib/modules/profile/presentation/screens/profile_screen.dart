import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';
import 'package:transportation_manager/modules/create_trips/presentation/widgets/head_new_trip.dart';
import 'package:transportation_manager/modules/profile/presentation/widgets/app_bar_profile.dart';
import 'package:transportation_manager/modules/profile/presentation/widgets/item_widget.dart';
import 'package:transportation_manager/modules/profile/presentation/widgets/profile_picture.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics:const BouncingScrollPhysics(),
        child: Column(
          children: [
            const Stack(
              clipBehavior: Clip.none,
              children: [
                HeadNewTrip(),
                AppBarProfile(),
                ProfilePicture(),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 40.h),
              child: Text(
                'Name Name',
                style: Styles.nameSemiBold30,
              ),
            ),
            Text(
              eMail,
              style: Styles.semiBold16,
            ),
           const ItemWidget(text: lang, icon: Icons.language,),
           const ItemWidget(text: helpCenter, icon: Icons.help_center_outlined,),
           const ItemWidget(text: passwordManager, icon: Icons.lock_open,),
           const ItemWidget(text: logOut, icon: Icons.login_outlined,),
            SizedBox(
              height: 20.h,
            ),
          ],
        ),
      ),
    );
  }
}
