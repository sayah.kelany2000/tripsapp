import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:transportation_manager/core/utils/const_svg.dart';
import 'package:transportation_manager/core/utils/string_const.dart';
import 'package:transportation_manager/core/utils/styles.dart';

class AppBarProfile extends StatelessWidget {
  const AppBarProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: 55.h,
        left: 32.w,
        right: 32.w,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SvgPicture.asset(
            arrowBackOutLined,
          ),
          Text(
            profile.toUpperCase(),
            style: Styles.title18,
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              SvgPicture.asset(
                edit,
              ),
              SvgPicture.asset(
                edit2,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
