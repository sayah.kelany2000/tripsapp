import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/color.dart';

class ProfilePicture extends StatelessWidget {
  const ProfilePicture({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 100.h,
      left: 0,
      right: 0,
      child: Container(
        width: 180.w,
        height: 180.h,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: AppColors.kScaffoldBackGroundColor,
        ),
        child: Container(
          width: 160.w,
          height: 160.h,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: AssetImage(
                'assets/images/Mask Group 3.png',
              ),
            ),
          ),
        ),
      ),
    );
  }
}
