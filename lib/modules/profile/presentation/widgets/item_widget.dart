import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:transportation_manager/core/utils/color.dart';
import 'package:transportation_manager/core/utils/styles.dart';


class ItemWidget extends StatelessWidget {
  const ItemWidget({super.key,required this.text,required this.icon});
  final String text;
  final IconData icon;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.only(left: 22.w,right: 22.w,top: 19.h),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: AppColors.kDotesColor,
        ),
        child: Padding(
          padding:  EdgeInsets.only(left: 25.w,right: 21.w,top: 28.h,bottom: 28.h),
          child: Row(
            children: [
               Icon(
                icon,
                color: AppColors.kButtonColor,
              ),
              SizedBox(
                width: 12.w,
              ),
              Text(
                text,
                style: Styles.semiBold15,
              ),
              const Spacer(),
              const Icon(
                Icons.arrow_forward_ios,
                color: AppColors.kArrowColor,
              )
            ],
          ),
        ),
      ),
    );
  }
}
