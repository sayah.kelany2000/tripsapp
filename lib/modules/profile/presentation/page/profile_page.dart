import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transportation_manager/modules/profile/presentation/screens/profile_screen.dart';

class ProfilePage {
  static const String name = '/profile';
  static GetPage page = GetPage(
    name: name,
    transition: Transition.downToUp,
     curve: Curves.easeOutBack,
    transitionDuration: const Duration(milliseconds: 300),
    page: () => const ProfileScreen(),
  );
}
